//
// Course:     SE 2040
// Term:       Spring 2019
// Assignment: Lab 6: Hunt the Alien
// Author:     Jennifer
//

#include "map.h"
#include <iostream>
#include <ctime>
using namespace std;

/**
 * Class is used to create the map of which all entitys are placed and the game is played
 * Generates nullEntity cells and fills the mapCells with it, takes in a player to set their new x
 * and y location when they are set on the map
 */

/**
 * public method to call to generate the map, filling it with nullentitys, calls private helper methods
 */
void Map::generateMap(Player *player){
    srand(time(NULL));
    int numOfFaults = rand() % 3 + 1;
    int numOfBlockage = rand() % 3 + 3;
    for(int x = 0; x < 6; x++){
        for(int y = 0; y < 6; y++){
            NullEntity *entity = new NullEntity();
            cells[x][y] = new MapCell(x, y, entity->getSymbol(), entity);
        }
    }
    generateCells(numOfFaults, numOfBlockage, player);
}

/**
 * Method which takes in an entity and creates that number of entitys on the map
 */
void Map::loopGenerate(int numTimes, Entity *entity){
    int ranX, ranY;
    while(numTimes > 0){
        ranX = rand() % 6;
        ranY = rand() % 6;
        if(generateEntity(entity, ranX, ranY)){
            numTimes--;
        }
    }
}

/**
 * Method which creates that given entity at the given space if that space is a Nullentity
 *
 * @returns true if the entity was generated there, false otherwise
 */
bool Map::generateEntity(Entity *entity, int ranX, int ranY){
    bool returnVal = false;
    if(cells[ranX][ranY]->getSymbol() == '.'){
        cells[ranX][ranY] = new MapCell(ranX, ranY, entity->getSymbol(), entity);
        returnVal = true;
    } else {
        returnVal = false;
    }
    return returnVal;
}

/**
 * Main method which creates cells that are not NullEntity's
 */
void Map::generateCells(int numOfFaults, int numOfBlockage, Player *player){
    int ranX, ranY;

    loopGenerate(numOfFaults, new ElectricFault());
    loopGenerate(numOfBlockage, new Blockage());
    for(int i = 0; i < 3; i++){
        loopGenerate(1, new FlameThrower());
    }
    loopGenerate(1, new PowerLoader());
    loopGenerate(1, new Alien());


    int numPlayer = 1;
    while(numPlayer > 0){
       ranX = rand() % 6;
       ranY = rand() % 6;
       if(cells[ranX][ranY]->getSymbol() == '.'){
           cells[ranX][ranY]->enter(player);
           player->setLocation(ranX, ranY);
           numPlayer--;
       }
    }

}

/**
 * Prints out the map to the system, used for debugging
 */
void Map::displayMap(){
    for(int y = 0; y < 6; y++){
        for(int x = 0; x < 6; x++){
            cout << cells[x][y]->getSymbol() << " ";
        }
        cout << endl;
    }
}

Map::~Map(){
    for(int x = 0; x < 6; x++){
        for(int y = 0; y < 6; y++){
            delete cells[x][y];
        }
    }
}

void Map::operator =(Map *map){
    for (int i = 0; i < map->WIDTH; ++i){
        for (int j = 0; j < map->HEIGHT; ++j){
            cells[i][j] = map->cells[i][j];
        }
    }
}
