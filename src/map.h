//
// Course:     SE 2040
// Term:       Spring 2019
// Assignment: Lab 6: Hunt the Alien
// Author:     Jennifer
//


#ifndef SRC_MAIN_H_
#define SRC_MAIN_H_

#include "mapCell.h"
#include "electricFault.h"
#include "blockage.h"
#include "nullEntity.h"
#include "player.h"
#include "flamethrower.h"
#include "powerloader.h"
#include "alien.h"

class Map {
    static constexpr int WIDTH = 6;
    static constexpr int HEIGHT = 6;

public:
    MapCell *cells[WIDTH][HEIGHT];
    void operator = (Map *map);
    void generateMap(Player *player);
    void displayMap();
    ~Map();
private:
    void generateCells(int numOfFaults, int numOfBlokage, Player *player);
    bool generateEntity(Entity *entity, int x, int y);
    void loopGenerate(int numTimes, Entity *entity);

};





#endif /* SRC_MAIN_H_ */
