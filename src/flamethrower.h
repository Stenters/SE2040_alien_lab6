//
// Course:     SE 2040
// Term:       Spring 2019
// Assignment: Lab 6: Hunt the Alien
// Author:     Molly
//

#ifndef SRC_FLAMETHROWER_H_
#define SRC_FLAMETHROWER_H_

#include "entity.h"
#include "weapons.h"
#include <iostream>
using namespace std;


class FlameThrower : public Weapon {

public:
    FlameThrower(){
        symbol = 'F';
    }
    bool interact(Player *player);
    string getMsg();
    ~FlameThrower(){}
    char getSymbol() { return symbol; }

};



#endif /* SRC_FLAMETHROWER_H_ */
