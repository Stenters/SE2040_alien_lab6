/*
 * controller->cpp
 *
 *  Created on: Apr 26, 2019
 *      Author: uchtmanmn
 */

#include "controller.h"

Controller::Controller(Player *player, Map *map){
    this->player = player;
    this->map = map;
    this->map->generateMap(player);

}

bool Controller::run(){
    bool hasWon = false;

    while (player->isAlive()){
        this->move();

        if (player->hasWon()) {
            hasWon = true;
            player->killPlayer();
        }

    }

    return hasWon;
    }

void Controller::move(){
    bool valid = false;
    char direction;
    int destX, destY;

    int iMin = (player->getX() - 1) < 0 ? 0 : player->getX() - 1;
    int jMin = (player->getY() - 1) < 0 ? 0 : player->getY() - 1;
    int iMax = (player->getX() + 1) > 5 ? 5 : player->getX() + 1;
    int jMax = (player->getY() + 1) > 5 ? 5 : player->getY() + 1;

    while(!valid){
        destX = player->getX();
        destY = player->getY();

        for (int j = jMin; j <= jMax; ++j){
            for (int i = iMin; i <= iMax; ++i){
                if (map->cells[i][j]->getEntity()->getMsg() != ""){
                    cout << map->cells[i][j]->getEntity()->getMsg() << endl;
                }
            }
        }

        cout << endl << "Move N)orth, S)outh, E)ast, or W)est" << endl;
        cin >> direction;
        switch(direction){

            case 'n':
            case 'N':
                valid = checkMove(destX, --destY);
                break;

            case 's':
            case 'S':
                valid = checkMove(destX, ++destY);
                break;

            case 'e':
            case 'E':
                valid = checkMove(++destX, destY);
                break;

            case 'w':
            case 'W':
                valid = checkMove(--destX, destY);
                break;

            case 'd':
            case 'D':

                this->map->displayMap();
                break;

            default:
                cout << "Not a valid direction" << endl;
                break;
        }

    }
    MapCell *destCell = map->cells[destX][destY];

    /** TODO: change interact method to return true or false based on if player moves into a space?
               in which case, have internal calls to player->killPlayer()

               Fix bug where player dies after taking first step

               Test everything

               I changed the way debug works per lab reqs

               Make sure everything is up to sunff with lab reqs
   */

    if (!(destCell->getSymbol() == 'B') && (!player->hasPowerLoader() || player->hasFlameThrower())){
        player->toggleAlive(destCell->enter(player));
        map->cells[player->getX()][player->getY()]->vacate();
        player->setLocation(destX, destY);

    } else {
        cout << "The door is blocked" << endl;
    }
}

bool Controller::checkMove(int x, int y){
    bool retVal = x < 0 || x > 5 || y < 0 || y > 5;

    if (retVal) {
        cout << "Err, invalid location. Please choose another" <<  endl;
    }

    return !retVal;
}

