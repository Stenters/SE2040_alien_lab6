//
// Course:     SE 2040
// Term:       Spring 2019
// Assignment: Lab 6: Hunt the Alien
// Author:     Jennifer
//

#include "entity.h"

#ifndef SRC_MAPCELL_H
#define SRC_MAPCELL_H

class MapCell {
    int x;
    int y;
    char symbol;
    bool hasPlayer;
    Entity *object;

public:
    MapCell(int x, int y, char symbol, Entity *object);
    char display();
    bool enter(Player *player);
    void vacate();
    char getSymbol();
    Entity* getEntity();
    ~MapCell();

};

#endif /* SRC_MAPCELL_H */
