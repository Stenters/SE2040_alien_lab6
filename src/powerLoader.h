//
// Course:     SE 2040
// Term:       Spring 2019
// Assignment: Lab 6: Hunt the Alien
// Author:     Molly
//

#ifndef SRC_POWERLOADER_H
#define SRC_POWERLOADER_H

#include "entity.h"
#include "weapons.h"
#include <iostream>
using namespace std;


class PowerLoader : public Weapon{

public:
    PowerLoader(){
        symbol = 'P';
    }
    bool interact(Player *player);
    string getMsg();
    ~PowerLoader(){}
    char getSymbol(){return symbol;}
};



#endif
