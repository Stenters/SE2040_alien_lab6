//
// Course:     SE 2040
// Term:       Spring 2019
// Assignment: Lab 6: Hunt the Alien
// Author:     Molly
//

#include "player.h"
#include <iostream>
using namespace std;

Player::Player(){
    Player::numFlameThrowers = 0;
    Player::powerLoader = false;
    Player::alive = true;
    Player::win = false;
    xLocation = 0;
    yLocation = 0;
}


bool Player::hasFlameThrower(){
    return Player::numFlameThrowers > 0;
}

bool Player::hasPowerLoader(){
    return Player::powerLoader;
}

void Player::addFlameThrower(){
    Player::numFlameThrowers += 1;
}

void Player::setLocation(int x, int y){
    xLocation = x;
    yLocation = y;
}

void Player::addPowerLoader(){
    Player::powerLoader = true;
}

void Player::removeFlameThrower(){
    Player::numFlameThrowers -= 1;
}

bool Player::isAlive(){
    return alive;
}

void Player::killPlayer(){
    alive = false;
}

void Player::toggleAlive(bool living){
    Player::alive = living;
}

int Player::getX(){
    return xLocation;
}

int Player::getY(){
    return yLocation;
}

void Player::operator = (Player *player){
    this->numFlameThrowers = player->numFlameThrowers;
    this->powerLoader = player->powerLoader;
    this->alive = player->alive;
    this->xLocation = player->xLocation;
    this->yLocation = player->yLocation;
}
