//
// Course:     SE 2040
// Term:       Spring 2019
// Assignment: Lab 6: Hunt the Alien
// Author:     enterss
//


#ifndef SRC_NULLENTITY_H_
#define SRC_NULLENTITY_H_


#include "entity.h"
#include <iostream>
using namespace std;


class NullEntity : public Entity {

public:
    NullEntity() { symbol = '.'; }
    char getSymbol() { return symbol; }
    bool interact(Entity *player) { return true; }
    string getMsg() { return ""; }
    ~NullEntity(){}

};


#endif /* SRC_MAIN_H_ */
