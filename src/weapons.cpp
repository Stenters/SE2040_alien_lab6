/*
 * weapons.cpp
 *
 *  Created on: Apr 23, 2019
 *      Author: uchtmanmn
 */

#include "flamethrower.h"
#include "powerLoader.h"
#include "weapons.h"
#include <iostream>

using namespace std;

bool FlameThrower::interact(Player *player){
    if (symbol == 'F'){
        cout << "You find a flame thrower" << endl;
        player -> addFlameThrower();
        symbol = '.';
    }
    return player -> isAlive();
}

string FlameThrower::getMsg(){
    return "";
}

bool PowerLoader::interact(Player *player){
    if(symbol == 'P'){
        cout << "You find a power loader" << endl;
        player -> addPowerLoader();
        symbol = '.';
    }
    return player -> isAlive();
}

string PowerLoader::getMsg(){
    return "";
}

