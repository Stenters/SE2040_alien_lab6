//
// Course:     SE 2040
// Term:       Spring 2019
// Assignment: Lab 6: Hunt the Alien
// Author:     enterss
//

#ifndef SRC_ELECTRICFAULT_H_
#define SRC_ELECTRICFAULT_H_


#include "entity.h"
#include "danger.h"
#include <iostream>
using namespace std;


class ElectricFault : public Danger {

public:
    ElectricFault() { symbol = '@'; }
    char getSymbol() { return symbol; }
    bool interact(Player *player);
    string getMsg();
    ~ElectricFault(){}

};

#endif /* SRC_MAIN_H_ */
