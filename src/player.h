//
// Course:     SE 2040
// Term:       Spring 2019
// Assignment: Lab 6: Hunt the Alien
// Author:     Molly
//


#ifndef SRC_PLAYER_H_
#define SRC_PLAYER_H_

#include <iostream>
using namespace std;


class Player {
    int numFlameThrowers;
    bool powerLoader;
    bool alive;
    bool win;
    char token = '+';
    int xLocation;
    int yLocation;

public:
    Player();
    void operator = (Player *player);
    bool hasFlameThrower();
    bool hasPowerLoader();
    void addFlameThrower();
    void addPowerLoader();
    void removeFlameThrower();
    bool isAlive();
    bool hasWon() { return this->win; };
    void setHasWon(bool hasWon) { this->win = hasWon; }
    void killPlayer();
    void toggleAlive(bool living);
    void setLocation(int x, int y);
    int getX();
    int getY();
    ~Player(){}

};



#endif /* SRC_MAIN_H_ */
