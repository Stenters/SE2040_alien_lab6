//
// Course:     SE 2040
// Term:       Spring 2019
// Assignment: Lab 6: Hunt the Alien
// Author:     enterss
//


#include "alien.h"
#include "electricFault.h"
#include "danger.h"
#include <iostream>

#include "blockage.h"
using namespace std;

/**
 * Kill the player if they don't have the proper weapon
 * Die otherwise
 *
 * @param player a reference to the player
 * @return true if the player survives, false if they die
 */

// --- Alien ---
bool Alien::interact(Player *player){
    bool retVal = true;


    if (player->hasPowerLoader()){
        cout << "You crush the alien to death" << endl;

        isDead = true;
        player->setHasWon(true);

    } else if (player->hasFlameThrower()){
        cout << "You blast the alien with a flamethrower" << endl;

        isDead = isDamaged;
        isDamaged = true;
        player->removeFlameThrower();
        player->setHasWon(true);

    } else {
        cout << "The alien horribly mauls you" << endl;
        retVal = false;
    }

    return retVal;
}


string Alien::getMsg() {
    return "You smell a sharp tang in the air";
}

// --- END Alien --
// --- Blockage ---

bool Blockage::interact(Player *player){
    if (player->hasPowerLoader()){
        cout << "You smash away the beams" << endl;

    } else if (player->hasFlameThrower()){
        cout << "You burn through the steel beams" << endl;
        player->removeFlameThrower();

    } else {
        cout << "This door is blocked" << endl;
    }
    return true;
}

string Blockage::getMsg() {
    return "The door is blocked by steel beams";
}

// --- END Blockage ---
// --- ElectricFault ---

bool ElectricFault::interact(Player *player){
    cout << "You get horribly electrocuted" << endl;
    return false;
}

string ElectricFault::getMsg() {
    return "The air smells like ozone";
}

// --- END ElectricFault ---
