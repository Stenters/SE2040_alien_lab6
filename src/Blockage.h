//
// Course:     SE 2040
// Term:       Spring 2019
// Assignment: Lab 6: Hunt the Alien
// Author:     enterss
//

#ifndef SRC_BLOCKAGE_H
#define SRC_BLOCKAGE_H


#include "entity.h"
#include "danger.h"
#include <iostream>
using namespace std;


class Blockage : public Danger {

public:
    Blockage() { symbol = 'B'; }
    bool interact(Player *player);
    string getMsg();
    ~Blockage(){}

};


#endif /* SRC_MAIN_H_ */
