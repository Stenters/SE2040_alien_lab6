//
// Course:     SE 2040
// Term:       Spring 2019
// Assignment: Lab 6: Hunt the Alien
// Author:     Jennifer
//

#include "mapCell.h"
#include <string>
using namespace std;

/**
 * One cell of the overarching map structure, takes in an entity which defines what the cell is.
 */

MapCell::MapCell(int x1, int y1, char symbol1, Entity *object1){
    x = x1;
    y = y1;
    symbol = symbol1;
    object = object1;
    hasPlayer = false;
}

/**
 * Displays what the entity is
 */
char MapCell::display(){
    return symbol;
}

/**
 * Tells the mapCell that a player is currently in the cell and for them to interact with that cell
 */
bool MapCell::enter(Player *player){
    hasPlayer = true;
    return object->interact(player);
}

/**
 * Resets the symbol and sets that there is no longer a player in the map
 */
void MapCell::vacate(){
    symbol = object->getSymbol();
    hasPlayer = false;
}

/**
 * Prints out the entitys symbol and otherwise prints out the player symbol if they are currently there
 */
char MapCell::getSymbol(){
    if(hasPlayer){
       return '+';
    }else {
        return symbol;
    }
}

/**
 * Returns the mapCells entity
 */
Entity* MapCell::getEntity(){
    return object;
}

MapCell::~MapCell(){
    delete this;
}
