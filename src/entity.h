//
// Course:     SE 2040
// Term:       Spring 2019
// Assignment: Lab 6: Hunt the Alien
// Author:     enterss
//


#ifndef SRC_ENTITY_H_
#define SRC_ENTITY_H_

#include "player.h"
#include <iostream>
using namespace std;

class Entity {
protected:
    char symbol;

public:

    /**
     * Constructor for entity
     */
    Entity(){ symbol = 0; }

    /**
     * Method for interacting with the player
     *
     * @param player a reference to the player
     * @return true if the player survives, false if the player dies
     */
    virtual bool interact(Player *player) { return true; }

    /**
     * Method for getting the message of an entity to
     * give hints to the player
     *
     * @return a string containing the message
     */
    virtual string getMsg() { return ""; }

    /**
     * Method for obtaining the symbol of the entity
     */
    virtual char getSymbol() { return symbol; }

    /**
     * Operator for comparing two entities
     */
    virtual bool operator = (Entity* otherEntity){
        return symbol == otherEntity->symbol;
    }

    /**
     * Destructor for entity
     */
    virtual ~Entity(){}

};


#endif /* SRC_MAIN_H_ */
