//
// Course:     SE 2040
// Term:       Spring 2019
// Assignment: Lab 6: Hunt the Alien
// Author:     enterss
//


#ifndef SRC_ALIEN_H_
#define SRC_ALIEN_H_

#include "entity.h"
#include "danger.h"
#include <iostream>
using namespace std;


class Alien : public Danger{
    bool isDamaged;
    bool isDead;

public:
    Alien() {
        symbol = '!';
        isDead = false;
        isDamaged = false;
    }

    char getSymbol() { return symbol; }
    bool interact(Player *player);
    string getMsg();
    ~Alien(){}

};



#endif /* SRC_MAIN_H_ */
