/*
 * controller.h
 *
 *  Created on: Apr 26, 2019
 *      Author: uchtmanmn
 */

#ifndef SRC_CONTROLLER_H_
#define SRC_CONTROLLER_H_

#include "entity.h"
#include "danger.h"
#include "alien.h"
#include "blockage.h"
#include "electricFault.h"
#include "flameThrower.h"
#include "map.h"
#include "mapCell.h"
#include "nullEntity.h"
#include "player.h"
#include "powerLoader.h"
#include "weapons.h"
#include <iostream>
using namespace std;

class Controller{
    Player *player;
    Map *map;

public:
    Controller(Player *player, Map *map);
    bool run();
private:
    void messages();
    void printMap();
    void move();
    void interact();
    bool checkMove(int x, int y);
};



#endif /* SRC_CONTROLLER_H_ */
