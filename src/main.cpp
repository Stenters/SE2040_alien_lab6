//
// Course:     SE 2040
// Term:       Spring 2019
// Assignment: Lab 6: Hunt the Alien
// Author:     Jennifer
//

#include "map.h"
#include "controller.h"
#include <string>
#include <iostream>
#include <fstream>
using namespace std;

int main(int argc, char *argv[]) {
    char userInput;

	do {
	    Controller *controller = new Controller( new Player(), new Map());

	    if (controller->run()){
	        cout << "You won! Play again? (Y or N)" << endl;
	    } else {
	        cout << "You lost! Play agian? (Y or N)" << endl;
	    }

	    cin >> userInput;

	} while (userInput == 'y' || userInput == 'Y');

	return 0;
}
